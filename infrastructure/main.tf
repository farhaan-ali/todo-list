terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.3.0"
    }
  }
  required_version = ">=0.14"
}

variable "gitlab_token" {
  sensitive = true
}

provider "gitlab" {
  token = var.gitlab_token
}

data "gitlab_user" "farhaan-ali" {
  username = "farhaan-ali"
}

resource "gitlab_project" "todo-list" {
  name                                  = "todo-list"
  description                           = "Practise using work tooling to create a todo-list"
  visibility_level                      = "public"
  default_branch                        = "master"
  wiki_enabled                          = false
  snippets_enabled                      = false
  merge_method                          = "ff"
  only_allow_merge_if_pipeline_succeeds = true
  merge_requests_enabled                = true
  issues_enabled                        = false
  request_access_enabled                = false
  lfs_enabled                           = false
  shared_runners_enabled                = true
  packages_enabled                      = false
}

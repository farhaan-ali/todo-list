# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.3.0"
  constraints = "3.3.0"
  hashes = [
    "h1:QN4F2qvLmIch/4B0vO477vuIYh7NFnNjiQ7Jw/LJAYg=",
    "zh:1e41b566dd2a1dd8cdb4ee35e4821c453f079499738ce3229e8f1ca2ad312eb1",
    "zh:3aecc2c975f84452c8eb46eaff14fe7cd95b6f31aaee739f61ebfb02fef265ff",
    "zh:3c1c57526a1867893587d8bdb8c643ce57c27f3490100c93e63ee66398eec475",
    "zh:419d4172b0eef4cc7a27f7c15e790442f595aaa87ef54362cd9578cae7b9b55d",
    "zh:5de73c23e8a1273fb6c62f1c6683e7a2983b00a5a295b3f793115b7cd198a189",
    "zh:6e8c4772dfc7c4c7a455abafcb27eae24b7fe315259d94b3a8f7fb648b12eefa",
    "zh:b6e999f00d4e53a143ca339af165ec68833c63623b05f6840a4384ef2bcc70dc",
    "zh:bab4ee5cf949db4c76192a878aeb04fc9a061c7c783595ff7aff2a6ef35ddce7",
    "zh:c9d65cea47686ff37efa03be8e88132f86a5516a9154fa1c99160bed0c582a9d",
    "zh:fb6b9250a2f9ebaf21b6b1a15032b79a3c0fa64897e4498ea0db589554ee61e5",
    "zh:fcbea1ec35308706e5795f2360c82dfe51c556677d8ad2249ed6cbb44e486cba",
  ]
}
